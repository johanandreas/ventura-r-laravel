# Ventura-r-Laravel 

A Ventura-r-Laravel project remake from lumen migrate to laravel 8, this is just another complicated project
for more question about this project you can ask to:

* For Gitlab Profile: [Johan Andreas Pranoto](https://gitlab.com/johanandreas74)
* For Gmail: johanandreas74@gmail.com (Johan Andreas Pranoto)

## Requirement
* Docker >= 4.0
* PHP >= 7.3
* Composer >= 2.1.12
* Laravel >= 8

For how to install Docker, you can go through here first:
* Windows
```
https://docs.docker.com/desktop/windows/install/
```
* Mac
```
https://docs.docker.com/desktop/mac/install/
```

Make sure your docker run completely fine(WSL2 enable, etc...)

## Getting Started

We using laravel with docker and as a server database we use MySQL. for minimize development we put all our need to docker.

## How to Use

**Step 1:**

Clone this [repo](https://gitlab.com/johanandreas/ventura-r-laravel)

**Step 2:**

Open Visual Studio and go to project root directory and execute the following command in console to get the required dependencies, this may take a while for installing:

```
docker-compose up -d --build
```

then you can type this command on root project directory:

```
docker-compose exec php php var/www/html/artisan migrate
```

**Step 3:**
after successfully creating 3 image and migrate above steps, you can start your working now!, for stopping remove container docker  process you can use this:

```
docker-compose down
```
COMMON USE you can use this:
```
docker-compose stop
```


**Step 4:**

Our database using mySQL, so you can download the UI through [here](https://dbeaver.io/download/) dbeaver, or using MySQL workbench [here](https://www.mysql.com/downloads/). After that create database named 'ventura'


**Step 5:**

By default port database exposed at port 3306 (development purpose). make sure you connect it.

ex:

Server host: localhost

port: 3306

user: admin/root(depend on your config)

password: admin(depend on your config)

Press test connection. and finish if you dont have trouble with it.

**Step 6**

copy .env.example and paste in main-app/. Rename it as .env

**Step 7**

Change config value according what i mentioned

DB_HOST=mysql

DB_PORT=3306

DB_DATABASE=ventura

DB_USERNAME=root/admin

DB_PASSWORD=admin(depend on your config)
