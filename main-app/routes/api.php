<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
  'prefix' => 'auth'
], function () {
  Route::post('/register', [AuthController::class, 'register'])->name('api.auth.register');
  Route::post('/login', [AuthController::class, 'login']);
  Route::post('/logout', [AuthController::class, 'logout']);
  // Route::group([
  //   'middleware' => 'authMember'
  // ], function () {

  //   Route::get('/logout', 'AuthController@logout');
  // });

  Route::middleware(['auth:sanctum'])->group(function () {

    Route::post('login-verify',  [AuthController::class, 'loginVerify']);
  });
});

Route::group([
  'prefix' => 'projects'
], function () {

  Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('create-project', [ProjectController::class, 'create']);
    // Route::post('login-verify',  [AuthController::class, 'loginVerify']);
  });
});



Route::get('/success', function () {
  return "Pembayaran Berhasil";
})->name('api.xendit.pay.success');
