<?php

namespace App\Http\Controllers\Api;

use App\Entities\Headers;
use App\Exceptions\ErrorException;
use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Api\AuthService;

/** 
 * @group Client API Authentication
 */
class AuthController extends Controller
{
  protected $authService;

  public function __construct(AuthService $authService)
  {
    $this->authService = $authService;
  }

  /** 
   * Login
   * 
   * Login user with idn mobile phone. Another Login Code can't be requested until holding_time has run out.
   * 
   * @bodyParam phone string required idn phone. Example: +6282163085454
   * @bodyParam signature string. Example: m83je89sVRs
   * 
   * @response {
   *  "code": 200,
   *  "message": "string message",
   *  "holding_time" : "int (seconds)"
   * }
   */
  public function login(Request $request)
  {
    $credentials = $request->only('name', 'password');

    //valid credential
    $validator = Validator::make($credentials, [
      'name' => 'required|string',
      'password' => 'required|string|min:6|max:50'
    ]);

    //Send failed response if request is not valid
    if ($validator->fails()) {
      return response()->json(['error' => $validator->errors()], 400);
    }

    return $this->authService->authenticate($credentials);
  }

  /**
   * Verify Login
   * 
   * verifying user login attempt. response **token** if user have registered or response
   * **registration_token** if user have to register 
   * 
   * @bodyParam phone string required idn phone. Example: +6282163085454
   * @bodyParam code string required 6 digit code. Example: 123456
   * 
   * @response {
   *  "code": 200,
   *  "token": "string token"
   * }
   * 
   * @response {
   *  "code": 200,
   *  "registration_token": "string registration token"
   * }
   */
  public function loginVerify(Request $request)
  {
    
      $user =  $this->authService->getUser();

      return $this->responsePayload([
        'user' => $user
      ]);
  }

  /** 
   * Register User
   * 
   * register user with registration_token from verify phone result
   * 
   * @bodyParam registration_token string required registration token
   * @bodyParam name string required user name
   * @bodyParam email string required user email
   * 
   * @response {
   *  "code": 200,
   *  "token": "string token"
   * }
   */
  public function register(Request $request)
  {
    $validator = Validator::make($request->post(), [
      // 'registration_token' => 'required|string',

      'name' => 'required|string|min:3',
      'email' => 'required|string',
      'phone_number' => 'required|string',
      'password' => 'required|string'
      //TODO: CUSTOM VALIDATION CHECK
      // 'name' => 'required|string|min:3|alpha_spaces',
      // 'email' => 'required|email_non_blacklist|unique:email_verified,email'
    ]);

    if ($validator->fails()) {
      throw new ErrorException($validator->errors()->first());
    }

    $name = $request->input('name');
    $password = $request->input('password');
    $email = $request->input('email');
    $phoneNumber = $request->input('phone_number');
    // $memberDeviceId = $this->getMemberDeviceId($request);

    // $ipAddress = Helper::getClientIp($request);

    $result = $this->authService->register($name, $password, $phoneNumber, $email);
    return $this->responsePayload($result);
  }

  /** 
   * Logout 
   * 
   * @authenticated
   * 
   * log user out
   *
   * @response {
   *  "code": 200,
   *  "message": "string message"
   * } 
   */
  public function logout(Request $request)
  {
    $userId = $request->input('user_id');
    $this->authService->logout($userId);

    //TODO: message translator
    // return $this->responseSuccess(trans('api_msg.logout'));

    return $this->responseSuccess('success logout');
  }

}
