<?php

namespace App\Http\Controllers\Api;
use App\Entities\Headers;
use App\Exceptions\ErrorException;
use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Api\AuthService;
use App\Services\Api\ProjectService;
use App\Models\User;

/** 
 * @group Client API Authentication
 */
class ProjectController extends Controller
{
  protected $authService;
  protected $projectService;

  public function __construct(AuthService $authService, ProjectService $projectService)
  {
    $this->projectService = $projectService;
    $this->authService = $authService;
  }

  /** 
   * Register User
   * 
   * register user with registration_token from verify phone result
   * 
   * @bodyParam registration_token string required registration token
   * @bodyParam name string required user name
   * @bodyParam email string required user email
   * 
   * @response {
   *  "code": 200,
   *  "token": "string token"
   * }
   */
  public function create(Request $request)
  {
    $validator = Validator::make($request->post(), [
      'name_proyek' => 'required|string|min:3',
      'alamat' => 'required|string',
      'start_date' => 'required|date|date_format:Y-m-d',
      'end_date' => 'required|date|date_format:Y-m-d|after:start_date',
      'project_detail' => 'required|array',
      'project_detail.*.user_id' => 'required|integer',
      //TODO: CUSTOM VALIDATION CHECK
      // 'name' => 'required|string|min:3|alpha_spaces',
      // 'email' => 'required|email_non_blacklist|unique:email_verified,email'
    ]);

    if ($validator->fails()) {
      throw new ErrorException($validator->errors()->first());
    }

    $created_by = $this->authService->getUser();

    $response = $this->projectService->createProject($request, $created_by);
    
    return $this->responseSuccess($response);
  }
}
