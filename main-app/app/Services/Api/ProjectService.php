<?php

namespace App\Services\Api;

use App\Models\User;
use App\Models\Projects;
use App\Models\ProjectDetail;
use App\Exceptions\ErrorException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class ProjectService
{

  public function __construct()
  {
    
  }

  public function createProject(Request $data, User $createdBy)
  {

    $name_proyek = $data->post('name_proyek');
    $alamat = $data->post('alamat');
    $start_date = $data->post('start_date');
    $end_date = $data->post('end_date');
    $projectDetail = $data->post('project_detail');

    DB::beginTransaction();
    try {
      $project = Projects::create(
        Projects::newRecord(
          $name_proyek,
          $alamat,
          $start_date,
          $end_date,
          1,
          null,
          $createdBy->id,
          null
        )
      );

      $detail = [];

      foreach ($projectDetail as $key => $value) {
        $detail [] = ProjectDetail::newRecord($project->id, $value['user_id']);
      }
      ProjectDetail::insert($detail);
      DB::commit();
      return "Sukses Menambahkan Project";
    } catch (\Exception $e) {
      DB::rollBack();
      throw new ErrorException($e->getMessage());
    }
  }

}
