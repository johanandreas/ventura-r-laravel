<?php

namespace App\Services\Api;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthService
{
  public function __construct()
  {
  }

  public function authenticate($credentials)
  {

    $user = User::getMember($credentials['name']);

    if (!$user) {
      return response()->json(
        [
          "code" => 400,
          "message" => 'User Not Found'
        ],
        400
      );
    }

    if (Hash::check($credentials['password'], $user->password)) {
      $plainToken = $user->createToken('auth_token')->plainTextToken;

      $token = explode("|", $plainToken);

      return response()->json(
        [
          'id' => $user->id,
          'user' => $credentials['name'],
          'token' => $token[1],
          'sequence' => $token[0],
          'code' => 200
        ],
        200
      );
    }
    return response()->json(
      [
        "code" => 400,
        "message" => 'password doesnt match'
      ],
      400
    );
  }

  public function register(string $name, string $password, string $phone, string $email)
  {
    $user = User::create(
      User::newRecord($name, $password, $phone, $email, 1, 0)
    );

    if ($user) {
      return [
        'code' => 200,
        'messages' => 'Success Register'
      ];
    }

    return [
      'code' => 400,
      'messages' => 'Gagal Register'
    ];
  }

  public function logout($userId)
  {
    $user = User::find($userId)->first();

    return $user->tokens()->delete();
  }

  public function getUser()
  {

    return Auth::user();
    // return "babik";
  }
}
