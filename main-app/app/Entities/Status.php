<?php

namespace App\Entities;

class Status
{
  const ACTIVE = 1;
  const INACTIVE = 0;
}