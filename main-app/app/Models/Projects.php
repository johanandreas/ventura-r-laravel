<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    //for testing
   // use HasFactory;
    use ModelTrait;
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name_proyek',
        'alamat',
        'start_date',
        'end_date',
        'note',
        'created_by',
        'edited_by',
        'status',
        'created_at',
        'updated_at'
    ];

    public static function newRecord(string $projectName, string $alamat, string $startDate, string $endDate, int $status, string $note = null, int $createdBy, int $editedBy = null): array
    {
      $now = date('Y-m-d H:i:s');
  
      return [  
        'name_proyek' => $projectName,
        'alamat' => $alamat,
        'start_date' => $startDate,
        'end_date' => $endDate,
        'status' => $status,
        'created_at' => $now,
        'updated_at' => $now,
        'note' => $note,
        'created_by' => $createdBy,
        'edited_by' => $editedBy
      ];
    }
}
