<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model
{
    // use HasFactory;
    use ModelTrait;
    protected $table = 'project_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'project_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public static function newRecord(int $projectId, int $userId): array
    {
      $now = date('Y-m-d H:i:s');
  
      return [
        'project_id' => $projectId,
        'user_id' => $userId,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }
}
