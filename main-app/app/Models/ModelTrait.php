<?php

namespace App\Models;

trait ModelTrait
{
  public static function updateWhere(array $data, $whereValue, $whereField = 'id')
  {
    return self::where($whereField, $whereValue)
      ->update($data);
  }

  public static function deleteWhere($whereValue, $whereField = 'id')
  {
    return self::where($whereField, $whereValue)
      ->delete();
  }
}
