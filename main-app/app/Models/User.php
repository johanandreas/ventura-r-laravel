<?php

namespace App\Models;

use app\Entities\Status;
use app\Entities\UserLevel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'user_level_id',
        'status_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
  
    public static function newRecord(
        string $name,
        string $password, 
        string $phone, 
        string $email, 
        int $userLevel, 
        int $statusId): array
    {
        $status = $statusId ? Status::ACTIVE : 0;
        $now = date("Y-m-d H:i:s");

        return [
        'name' => $name,
        'password' => Hash::make($password),
        'phone' => $phone,
        'email' => $email,
        'user_level_id' => $userLevel,
        'email_verified_at' => $now,
        'status_id' => $status,
        'created_at' => $now,
        'updated_at' => $now
        ];
    }
    
    public static function getMember($name){
        
        return self::where('name', $name)
        ->first();
    }

    public static function updateToken($id, $token){
        return self::where('id', $id)
        ->update(['remember_token' => $token]);
    }
}
