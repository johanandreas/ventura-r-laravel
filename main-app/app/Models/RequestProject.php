<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestProject extends Model
{
    // use HasFactory;

    protected $table = 'request_project';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'project_id',
        'date_request',
        'note',
        'status',
        'created_at',
        'updated_at'
    ];

    public static function newRecord(int $projectId, string $dateRequest, string $note, int $status): array
    {
      $now = date('Y-m-d H:i:s');
  
      return [
        'project_id' => $projectId,
        'date_request' => $dateRequest,
        'note' => $note,
        'status' => $status,
        'created_at' => $now,
        'updated_at' => $now
      ];
    }
}
