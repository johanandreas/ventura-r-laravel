<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestProjectList extends Model
{
    // use HasFactory;

     protected $table = 'request_project_list';

     /**
      * The attributes that are mass assignable.
      *
      * @var string[]
      */
     protected $fillable = [
         'request_project_id',
         'item_id',
         'qty',
         'note',
         'created_at',
         'updated_at'
     ];
 
     public static function newRecord(int $requestProjectId, int $itemId, int $qty, string $note): array
     {
       $now = date('Y-m-d H:i:s');
   
       return [
         'request_project_id' => $requestProjectId,
         'item_id' => $itemId,
         'qty' => $qty,
         'note' => $note,
         'created_at' => $now,
         'updated_at' => $now
       ];
     }
}
