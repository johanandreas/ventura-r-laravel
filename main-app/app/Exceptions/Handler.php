<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request, $exception)
    {
        // if ($exception instanceof UnauthorizedHttpException) {
        //     $preException = $exception->getPrevious();

        //     $errMessage = '';
        //     //for detail exception
        //     if ($preException instanceof
        //         \Tymon\JWTAuth\Exceptions\TokenExpiredException
        //     ) {
        //         $errMessage = 'TOKEN_EXPIRED';
        //     } else if ($preException instanceof
        //         \Tymon\JWTAuth\Exceptions\TokenInvalidException
        //     ) {

        //         $errMessage = 'TOKEN_INVALID';
        //     } else if ($preException instanceof
        //         \Tymon\JWTAuth\Exceptions\TokenBlacklistedException
        //     ) {
        //         $errMessage = 'TOKEN_BLACKLISTED';
        //     }
        //     if ($exception->getMessage() === 'Token not provided') {
        //         $errMessage = 'Token not provided';
        //     }

        //     return response()->json([
        //         'message' => 'Unauthorized',
        //         'error' => $errMessage,
        //     ], 401);
        // }
        if($exception instanceof ErrorException){
            return response()->json(['message' => $exception->getMessage()], 400);
        }

        return parent::render($request, $exception);
        // return $this->prepareJsonResponse($request, $exception);
    }
}
