<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('password');
            $table->integer('user_level_id')->nullable();
            $table->integer('user_group_id')->nullable();
            $table->string('phone', 20)->unique();
            $table->string('email');
            $table->integer('status_id')->unsigned();
            $table->dateTime('email_verified_at')->nullable();
            $table->boolean('active')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_level', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jabatan')->unique();
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('user_access_menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_level_id')->constrained('user_level');
            $table->string('route_menu');
            $table->timestamps();
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_level');
        Schema::dropIfExists('user_access_menu');
    }
}