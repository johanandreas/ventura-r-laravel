<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyekTable extends Migration
{
    /**
     * docker-compose exec php php /var/www/html/artisan migrate:refresh --path=/database/migrations/2021_11_27_065926_create_proyek_table.php
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * 
         * proyek bisa banyak mandor.
         * 
         */
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_proyek', 100);
            $table->string('alamat');
            $table->string('note',255)->nullable();
            $table->integer('created_by');
            $table->integer('edited_by')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('status');
            $table->timestamps();
        });

        Schema::create('project_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('project_id')->constrained('projects'); 
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });

        //TODO:
        //pengajuan barang ada 3 mode
        //SURAT JALAN, PENGAMBILAN BARANG, PENGAJUAN BARANG

        Schema::create('request_project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id');
            $table->date('date_request');
            $table->string('note',255);
            $table->string('status');
            $table->timestamps();
        });

        Schema::create('request_project_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('request_project_id');
            $table->integer('item_id');
            $table->integer('qty');
            $table->string('note', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_detail');
        Schema::dropIfExists('projects');
        Schema::dropIfExists('request_project_list');
        Schema::dropIfExists('request_project');
    }
}
