FROM php:8.0.13-fpm-alpine
# Install PHP extensions
# RUN docker-php-ext-install pdo pdo_mysql
RUN apt-get update

# Install Postgre PDO
RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql